import { Injectable } from '@angular/core';
// import { Observable } from 'rxjs/Observable';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {

  apiUrl: string = 'http://fm.fayweb.mx/api';
  // apiUrl: string = 'http://192.168.5.173:8000/api';
  apiToken: string = null;
  // apiToken: string = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQ5YTNiNjU2OTM3ZDdiYjczMTczZTAxNWUzNjRhODFhYjk2MjhkMWYxOGQ4Y2MyNzhhODFmZjNkNzgyZGUxZmNjNGU3MmM3NjBmMjgxYzE3In0.eyJhdWQiOiIxIiwianRpIjoiNDlhM2I2NTY5MzdkN2JiNzMxNzNlMDE1ZTM2NGE4MWFiOTYyOGQxZjE4ZDhjYzI3OGE4MWZmM2Q3ODJkZTFmY2M0ZTcyYzc2MGYyODFjMTciLCJpYXQiOjE0OTYyNjQwODAsIm5iZiI6MTQ5NjI2NDA4MCwiZXhwIjoxNTI3ODAwMDgwLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.L_vmIkO4I7pFg8YYAz7U4bsqfgUM4ahUvm6XdAcU-SfNaJaQ3visDwvp-ZPRidvFEH3XpL7GclBVZvwd3Irf7NQ-8lfL1EaxsA1prcvtEaWktQK7J1ICduDD8dznhJqxJQG26vX2gbq7r7UeUDNKypCB7o0IoYzNmY6ojk3Ob4YxOrvSG1QfkAEdH4vqbk9kG337i5viR-W8gtZIZIhOUR7TkZWVomv3a6LFzsG1eBAYE7h-arb_GHvPiF1Ww9_0Cg248i_ttOKz-I17RgzRXMxqtkRUHglFBXhb1kQUzKrQrSycIGhLJt818hrR8ILvyAQGj4UdCNmIxEdSZX-vPsoSSExpPsDZLlhxTr3aCQ5s_rprT206PaX5ydQg_hoIp00DefzG_FY45QL7tMpnCdC4WjkATHfYQorWewyuvaw92L9IXwxiMCLqCLQqQ2gJeSgXauY8sUoYun2dMsQyZCoSh1lP4SQfXpG_lkTIdFj3zTvC1SWh96k0ORPuOhNBHOpWpryBGq7OukMUCck3V8ODQznXCtxPInBIo3JE3n3HXmTBv_oFw63JZOU0JY0C2VhY-U_SjL2PkjQDFWQW3oeTsLseqfy7H1AQ6ZR5rO23VTm6q1f2LDtHMxXI5yQStLPR2raiCfMJnl7z9E3XfzP-i2Lru3HK_4VR5toa_2E';
  header: Headers = new Headers();

  constructor(public http: Http) {
    this.apiToken = localStorage.getItem('userToken');    
    if(this.apiToken != null) {
      this.header.set('Accept', 'application/json');
      this.header.set('Authorization', this.apiToken);
    }

  }

  getHeaders(){
    return this.header;
  }

  public conectar(user) {
    return new Promise((resolve, reject) => {
      this.http.post(this.apiUrl + '/user/check', user)
        .map(response => response.json())
        .subscribe(data => {
          this.header.set('Accept', 'application/json');
          this.header.set('Authorization', data['content']);
          resolve(data);
        },
        error => {
          reject(error);
        });
    });
  }

  public desconectar() {
    // localStorage.removeItem('currentUser');
    // localStorage.removeItem('userToken');
    // localStorage.removeItem('user');
    this.header.delete('Accept');
    this.header.delete('Authorization');
    localStorage.clear();
  }

  public registerUser(user) {
    return new Promise((resolve, reject) => {
      this.http.post(this.apiUrl + '/user/store', user)
        .map(response => response.json())
        .subscribe(data => {
          this.header.set('Accept', 'application/json');
          this.header.set('Authorization', data['content']);          
          resolve(data);
        }, error => {
          reject(error);
        });
    });
  }

  public setBodyType(type){
    return new Promise((resolve, reject) => {
      this.http.post(this.apiUrl + '/bodytype', type, {
        headers: this.getHeaders()
      })
        .map(response => response.json())
        .subscribe(data => {
          resolve(data);
        }, error => {
          reject(error);
        });
    });
  }

  public enviarColometria(data) {
    return new Promise((resolve, reject) => {
      this.http.post(this.apiUrl + '/colorimetria', data, {
        headers: this.getHeaders()
      })
        .map(response => response.json())
        .subscribe(data => 
          {
          resolve(data);
        },
        error => {
          reject(error);
        });
    });
  }

  public initTables(){
    return new Promise((resolve, reject) => {
      this.http.get(this.apiUrl + '/info_tables', {
        headers: this.getHeaders()
      })
        .map(response => response.json())
        .subscribe(data => {
          resolve(data);
        },
        error => {
          reject(error);
        });
    });
  }

  public getImages(subcategoria){
    return new Promise((resolve, reject) => {
      this.http.post(this.apiUrl + '/images', subcategoria, {
        headers: this.getHeaders()
      })
        .map(response => response.json())
        .subscribe(data => {
          resolve(data);
        },
        error => {
          reject(error);
        });
    });
  }

  public getTrends(){
    return new Promise((resolve, reject) => {
      this.http.get(this.apiUrl + '/trends', {
        headers: this.getHeaders()
      })
        .map(response => response.json())
        .subscribe(data => {
          resolve(data);
        },
        error => {
          reject(error);
        });
    });
  }

}