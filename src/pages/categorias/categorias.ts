import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { SubcategoriasPage } from '../subcategorias/subcategorias';

@Component({
  selector: 'page-categorias',
  templateUrl: 'categorias.html',
})
export class CategoriasPage {

  database: SQLite;
  categorias = [];
  param: number;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.param = this.navParams.get('param');
    let user = JSON.parse(localStorage.getItem('user'));
    let busqueda = null;
    if(user.sexo == 'm'){
      busqueda = 'h';
    } else {
      busqueda = 'm';
    }
    this.database = new SQLite();
    this.database.create({name: "fashionme.db", location: "default"}).then((db: SQLiteObject) => {
      db.executeSql("SELECT * FROM categorias WHERE genero != (?)", [busqueda]).then((data) => {
        for(let i = 0; i < data.rows.length; i++) {
            this.categorias.push({id: data.rows.item(i).id, categoria: data.rows.item(i).categoria, imgen: data.rows.item(i).imgen});
        }
      }, error => {
        alert('Acurrio un error: ' +error);
      });
    }, (error) => {
      let categorias = [
        {
            "id": 1,
            "categoria": "Vestidos",
            "imgen": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT9QrpmtGV1bxX7aSmAdGTCHcGCoAxjUigLxJ3RA-Bo9LkHYIaJ",
            "genero": "m"
        },
        {
            "id": 2,
            "categoria": "Tops",
            "imgen": "https://ae01.alicdn.com/kf/HTB1tKdfQpXXXXXGaXXXq6xXFXXXh/2017-Family-Matching-Outfits-T-Shirt-Tops-font-b-Clothes-b-font-Set-Man-Woman-Baby.jpg",
            "genero": "a"
        },
        {
            "id": 3,
            "categoria": "Bottoms",
            "imgen": "https://s-media-cache-ak0.pinimg.com/736x/34/db/a0/34dba0384e3752d957bed651e6a84f61--sport-outfits-nice-outfits.jpg",
            "genero": "a"
        },
        {
            "id": 4,
            "categoria": "Chamarras y Abrigos",
            "imgen": "https://http2.mlstatic.com/chamarra-unisex-hombre-mujer-parka-abrigo-envio-gratis-D_NQ_NP_414901-MLM20439606384_102015-F.jpg",
            "genero": "a"
        },
        {
            "id": 5,
            "categoria": "Lenceria",
            "imgen": "http://comps.canstockphoto.es/can-stock-photo_csp12005005.jpg",
            "genero": "m"
        },
        {
            "id": 6,
            "categoria": "Ropa de Baño",
            "imgen": "https://thumbs.dreamstime.com/x/colecci%C3%B3n-del-vector-del-traje-de-ba%C3%B1o-32627999.jpg",
            "genero": "a"
        },
        {
            "id": 7,
            "categoria": "Calzado",
            "imgen": "https://us.123rf.com/450wm/joycolor/joycolor1510/joycolor151000024/46959388-estilo-de-zapato-cl-sico-conjunto-de-hombre-de-cuero-zapatos-negros-y-zapatos-negros-de-cuero-mujer.jpg?ver=6",
            "genero": "a"
        },
        {
            "id": 8,
            "categoria": "Accesorios",
            "imgen": "http://lookandfashion-static.hola.com/hormasjumble/files/2013/04/Levis-SS-2013-002.jpg",
            "genero": "a"
        },
        {
            "id": 9,
            "categoria": "Ropa Interior",
            "imgen": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ9mFDQSm5RpL-ERLFK5vJKb4oaHhuX90TqbVs6dO9VWJLRlpmVnA",
            "genero": "h"
        }
      ];
        // categorias = JSON.parse(JSON.stringify(categorias));
        for (let item in categorias) {
            this.categorias.push({id: categorias[item].id, categoria: categorias[item].categoria, imgen: categorias[item].imgen});
        }
      console.error('No se pudo abrir la BD. ' +error);
    });
  }

  getSubCat(id, categoria){
    this.navCtrl.push(SubcategoriasPage, {id: id, categoria: categoria, closet: this.param});
  }


}
