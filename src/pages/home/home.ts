import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

// import { User, Data } from '../../database';
import { Register } from '../register/register';
import { Login } from '../login/login';
import { Tabs } from '../tabs/tabs';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

	data : any;
  registerPage = Register;
  loginPage = Login;
  currentUser: any;
  rootPage = Tabs;

  constructor(public navCtrl: NavController) {}

}
