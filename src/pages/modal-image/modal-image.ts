import {Component} from '@angular/core';
import {NavController, NavParams, ViewController} from 'ionic-angular';

@Component({
  selector: 'page-modal-image',
  templateUrl: 'modal-image.html',
})
export class ModalImagePage {

  img: string;
  title: string;
  id: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    this.id = this.navParams.get('id');
    this.img = this.navParams.get('img');
    this.title = this.navParams.get('title');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  addFavorite(id: number) {
    console.log(id);
  }
}
