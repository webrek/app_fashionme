import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { CategoriasPage } from '../categorias/categorias';

@Component({
  selector: 'page-closets',
  templateUrl: 'closets.html',
})
export class ClosetsPage {

  closet: string = "ideal";
  isAndroid: boolean = false;

  rootPage1 = CategoriasPage;
  param1 = 1;
  rootPage2 = CategoriasPage;
  param2 = 2;

  constructor(public navCtrl: NavController, public navParams: NavParams, public platform: Platform) {
  }

}
