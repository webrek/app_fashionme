import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ColorResultsPage } from './color-results';

@NgModule({
  declarations: [
    ColorResultsPage,
  ],
  imports: [
    IonicPageModule.forChild(ColorResultsPage),
  ],
  exports: [
    ColorResultsPage
  ]
})
export class ColorResultsPageModule {}
