import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
// import { ClosetsPage } from '../closets/closets';

@Component({
  selector: 'page-color-results',
  templateUrl: 'color-results.html',
})
export class ColorResultsPage {

  color: string = null;

  constructor(public navCtrl: NavController, public navParams: NavParams,  public viewCtrl: ViewController) {
    this.color = 'assets/images/'+this.navParams.get('color').toLowerCase()+'.jpg';
  }

  dismiss(){
    this.viewCtrl.dismiss();
    // this.navCtrl.setRoot(ClosetsPage);
  }

}
