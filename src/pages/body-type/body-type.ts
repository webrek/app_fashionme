import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController, Loading } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service';
import { Especificaciones } from '../especificaciones/especificaciones';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

@Component({
  selector: 'page-body-type',
  templateUrl: 'body-type.html',
})
export class BodyType {
  loading: Loading;
  img: string = this.navParams.get('img');
  tipo = {type: ''};
  cuerpo: string;
  database: SQLite;
  rootPage = Especificaciones;

  constructor(public navCtrl: NavController, public navParams: NavParams,
  private authService: AuthService, private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
    this.showLoading();
    let busqueda = null;
    let user = JSON.parse(localStorage.getItem('user'));
    if(user.sexo == 'm'){
      busqueda = 'Trapecio';
    } else {
      busqueda = 'Reloj de Arena';
    }
    this.database = new SQLite();

    this.database.create({name: "fashionme.db", location: "default"}).then((db: SQLiteObject) => {
      db.executeSql("SELECT * FROM cuerpos WHERE tipo_cuerpo != (?) ORDER BY RANDOM() LIMIT 1", [busqueda]).then((data) => {
        if(data.rows.length > 0) {
            for(let i = 0; i < data.rows.length; i++) {
                this.cuerpo = data.rows.item(i).tipo_cuerpo;
            }
        }
        this.tipo.type = this.cuerpo;
        this.authService.setBodyType(this.tipo).then((resultados) => {
          if(resultados['status'] == 'ok'){
            localStorage.setItem('user', JSON.stringify(resultados['user']));
          } else {
            this.showError(resultados['error']);
          }
        }, (error) => {
          this.showError('Algo salio mal, intenta más tarde.');
        });
      }, error => {
        console.error('Error del SQL: ' +JSON.stringify(error));
      });
    }, error => {
      console.error('Error openDB: ' +JSON.stringify(error));
    });
  }

  showLoading(){
  	this.loading = this.loadingCtrl.create({
  		content: 'Por favor espera...',
  		dismissOnPageChange: true
  	});
  	this.loading.present();
  }

  showError(error){
  	this.loading.dismiss();

  	let alert = this.alertCtrl.create({
  		title: 'Fallido',
  		subTitle: error,
  		buttons: ['Aceptar']
  	});
  	alert.present(prompt);
  }

}
