import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController, Loading, ViewController, ModalController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

// import { View } from '../tabs/tabs';
import { ClosetsPage } from '../closets/closets';
import { ColorResultsPage } from '../color-results/color-results';

@Component({
  selector: 'page-colorimetria',
  templateUrl: 'colorimetria.html',
})
export class Colorimetria {
  loading: Loading;
  formC: FormGroup;

  @ViewChild('changeIcon') changeIcon;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private authService: AuthService, 
    public formBuilder: FormBuilder,
    private alertCtrl: AlertController, 
    private loadingCtrl: LoadingController, 
    private viewCtrl: ViewController, 
    private modalCtrl: ModalController) {
    this.formC = this.cForm();
  }
  

  public cForm(){
    return this.formBuilder.group({
      ojos_tinte: ['', Validators.required],
      ojos_valor: ['', Validators.required],
      ojos_croma: ['', Validators.required],
      piel_tinte: ['', Validators.required],
      piel_valor: ['', Validators.required],
      piel_croma: ['', Validators.required],
      cabello_tinte: ['', Validators.required],
      cabello_valor: ['', Validators.required],
      cabello_croma: ['', Validators.required],
    });
  }

  enviarFormulario(){
    this.showLoading();
    this.authService.enviarColometria(this.formC.value).then((resultados) => {
      if(resultados['color']){
        let color = resultados['color'];
        let user = JSON.parse(localStorage.getItem('user'));
        if(color.tipo){
          alert('Ejecutar pregunta alterna');
        } else {
          user.color_id = color.id;
          localStorage.setItem('user', JSON.stringify(user));
          user = JSON.parse(localStorage.getItem('user'));
          if(user.cuerpo_id != null && user.color_id != null){
            this.navCtrl.remove(this.navCtrl.first().index).then( (result) => {
              let icon = document.getElementsByClassName('ion-md-camera').item(0);
              icon.classList.remove('ion-md-camera');
              icon.classList.add('ion-md-shirt');
              this.navCtrl.setRoot(ClosetsPage);
              this.presentColorRModal(resultados['color'].color);
            });
          }
        }
        this.loading.dismiss();
      } else {
        if(resultados['errors']){
          alert('Algo salio mal.');
        }
      }
    });
  }

  showLoading(){
  	this.loading = this.loadingCtrl.create({
  		content: 'Por favor espera...',
  		dismissOnPageChange: true
  	});
  	this.loading.present();
  }

  showError(error){
  	this.loading.dismiss();

  	let alert = this.alertCtrl.create({
  		title: 'Fallido',
  		subTitle: error,
  		buttons: ['Aceptar']
  	});
  	alert.present(prompt);
  }

  presentColorRModal(color) {
    let profileModal = this.modalCtrl.create(ColorResultsPage, {color: color});
    profileModal.present();
  }

}
