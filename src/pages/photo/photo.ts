import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Platform, ActionSheetController, ModalController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { BodyType } from '../body-type/body-type';

import { ClosetsPage } from '../closets/closets';

@Component({
  selector: 'page-photo',
  templateUrl: 'photo.html',
})
export class Photo {

  @ViewChild('pointsImage') canvas: any;
  elementoCanvas: any;
  image: string = null;

  contTap = 0;
  text_img = 'Imágenes';

  lastX: number;
  lastY: number;

  distancias: any = {hopbros: '', cintura: '', rodillas: ''};

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public actionsheetCtrl: ActionSheetController, public platform: Platform,
    public camera: Camera, public modalCtrl: ModalController) {
      let user = JSON.parse(localStorage.getItem('user'));
      if(user.cuerpo_id != null && user.color_id != null){
         this.navCtrl.setRoot(ClosetsPage);
      }
  }

  presentActionSheet() {
    let actionSheet = this.actionsheetCtrl.create({
      title: 'Albums',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Foto',
          icon: 'images',
          handler: () => {
            this.getImage();
          }
        },
        {
          text: 'Cámara',
          icon: 'camera',
          handler: () => {
            this.getPicture();
          }
        }
      ]
    });
    actionSheet.present();
  }

  getImage(){
    let cameraOptions = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      quality: 75,
      targetWidth: 800,
      targetHeight: 800
    }

    this.camera.getPicture(cameraOptions)
    .then(
      (file_uri) => {
        this.image = `data:image/jpeg;base64,${file_uri}`;
        localStorage.setItem('imageUser', this.image);
      },(err) => {
        console.log(err)
      }
    );
  }

  getPicture(){
    let options: CameraOptions = {
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      targetWidth: 1000,
      targetHeight: 1000,
      quality: 53
    }
    this.camera.getPicture( options )
    .then(imageData => {
      this.image = `data:image/jpeg;base64,${imageData}`;
      localStorage.setItem('imageUser', this.image);
    })
    .catch(error =>{
      console.error( error );
    });
  }

  getPosition(event){
    // this.contTap++;
    let position = event.center;
    console.log('X: ' + position.x, 'Y: ' + position.y);
    // if(this.contTap > 6){
    //   this.contTap = 1;
    // }
    // console.log(this.contTap);
  }

  getStart(){
   this.navCtrl.push(BodyType, {img: this.image});
  }

}
