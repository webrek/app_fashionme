import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ImagesPage } from '../images/images';

import { ProfilePage } from '../profile/profile';
import { CategoriasPage } from '../categorias/categorias';
import { TrendsPage } from '../trends/trends';

@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})
export class Welcome {

  profilePage = ProfilePage;
  enable = false;
  trends = TrendsPage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewWillEnter(){
    let user = JSON.parse(localStorage.getItem('user'));
    if(user.cuerpo_id != null && user.color_id != null){
      this.enable = false;
    } else {
      this.enable = true;
    }    
  }

  openNavDetailsPage(item) {
    this.navCtrl.push(ImagesPage, {id: 3});
  }

  openClosetIdeal(){
    this.navCtrl.push(CategoriasPage, {param: 1});
  }

  openClosetFM(){
    this.navCtrl.push(CategoriasPage, {param: 2});
  }


}
