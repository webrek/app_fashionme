import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController, Loading, ModalController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service';
import { ModalImagePage } from '../modal-image/modal-image';

@Component({
  selector: 'page-images',
  templateUrl: 'images.html',
})
export class ImagesPage {

  images = [];
  id = this.navParams.get('id');
  nombre = this.navParams.get('nombre');
  loading: Loading;
  closet: number = this.navParams.get('closet');

  constructor(public navCtrl: NavController, public navParams: NavParams,
  private auth: AuthService, public alertCtrl: AlertController,
  private loadingCtrl: LoadingController, public modalCtrl: ModalController) {
    this.showLoading();
    this.auth.getImages({categoria_id: this.id, closet: this.closet}).then((data) => {
      if(data['images']){
        this.images = data['images'];
        this.loading.dismiss();
      }
    }, error => {
      this.showError('Ocurrio un error: ' + error);
    });
  }

  showError(error) {
    this.loading.dismiss();
    let alert = this.alertCtrl.create({
      title: '¡UPS!',
      subTitle: error,
      buttons: ['Aceptar']
    });
    alert.present(prompt);
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Cargando imagenes, espere por favor...',
      dismissOnPageChange: false
    });
    this.loading.present();
  }


  openImage(image, title, id){
    let modal = this.modalCtrl.create(ModalImagePage, {img: image, title: title, id: id});
    modal.present();
  }

}
