import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Loading } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../providers/auth-service';
import { Tabs } from '../tabs/tabs';
// import { Data } from '../../database';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class Login {
 loading: Loading;
 formLogin: FormGroup;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public formBuilder: FormBuilder, 
    private auth: AuthService,  
    private alertCtrl: AlertController, 
    private loadingCtrl: LoadingController) {
  	 this.formLogin = this.createMyForm();
  }

  private createMyForm(){
    return this.formBuilder.group({
      data1: ['', Validators.required],
      data2: ['', Validators.required],
    });
  }

  public singIn(){
    this.formLogin.value.data1 = btoa(this.formLogin.value.data1);
    this.formLogin.value.data2 = btoa(this.formLogin.value.data2);
  	this.showLoading();
  	this.auth.conectar(this.formLogin.value).then((resultados) => {
      if(resultados['check'] == true){
        localStorage.setItem('currentUser', 'active');
        localStorage.setItem('userToken', resultados['content']);
        localStorage.setItem('user', JSON.stringify(resultados['user']));
        this.navCtrl.setRoot(Tabs);
      } else {
        this.formLogin = this.createMyForm();
        this.showError('Correo o contraseña incorrectos.');
      }
    }, (error) => {
        this.showError('Algo salió mal con la conexón. Intenta más tarde.');
    });
  }

  showLoading(){
  	this.loading = this.loadingCtrl.create({
  		content: 'Por favor espera...',
  		dismissOnPageChange: true
  	});
  	this.loading.present();
  }

  showError(error){
  	this.loading.dismiss();

  	let alert = this.alertCtrl.create({
  		title: '¡UPS!',
  		subTitle: error,
  		buttons: ['Aceptar']
  	});
  	alert.present(prompt);
  }

}
