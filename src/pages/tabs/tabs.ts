import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController, Loading, ViewController, ModalController } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

import { AuthService } from '../../providers/auth-service';

import { HomePage } from '../home/home';
import { Welcome } from '../welcome/welcome';
import { Photo } from '../photo/photo';
import { ClosetsPage } from '../closets/closets';
import { StartPage } from '../start/start';

@Component({
  templateUrl: 'tabs.html',
})
export class Tabs {

  loading: Loading;
  tab1Root = Welcome;
  tab2Root = null;
  database: SQLite;
  user = JSON.parse(localStorage.getItem('user'));
  icon = 'camera';
  index: number;

  constructor(
    public navCtrl: NavController,
    private auth: AuthService,
    public alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private viewCtrl: ViewController,
    private modalCtrl: ModalController
  ) {
    if(this.user.color_id != null && this.user.cuerpo_id != null){
      this.icon = 'shirt';
      this.tab2Root = ClosetsPage;
      if(localStorage.getItem('viewStartPage') != 'true'){
        this.presentStartModal();
      }
    } else {
      this.icon = 'body';
      this.tab2Root = Photo;
      if(localStorage.getItem('viewStartPage') != 'true'){
        this.presentStartModal();
      }
    }
    if(this.user.image !== null && this.user.image !== "" && this.user.image){
      this.index = 0;
    } else {
      this.index = 1;
    }
    this.database = new SQLite();
    this.database.create({name: "fashionme.db", location: "default"}).then((db: SQLiteObject) => {
      let localData = { cuerpos: 0, categorias: 0, colores: 0, recomendaciones: 0, subcat: 0 };
      db.executeSql("SELECT * FROM categorias", []).then((data) => {
          localData.categorias = data.rows.length;
        }, error => {
          localData.categorias = 0;
      });
      db.executeSql("SELECT * FROM colores", []).then((data) => {
          localData.colores = data.rows.length;
        }, error => {
          localData.colores = 0;
      });
      db.executeSql("SELECT * FROM cuerpos", []).then((data) => {
          localData.cuerpos = data.rows.length;
        }, error => {
          localData.cuerpos = 0;
      });
      db.executeSql("SELECT * FROM recomendaciones", []).then((data) => {
          localData.recomendaciones = data.rows.length;
        }, error => {
          localData.recomendaciones = 0;
      });
      db.executeSql("SELECT * FROM subcategorias", []).then((data) => {
          localData.subcat = data.rows.length;
        }, error => {
          localData.recomendaciones = 0;
      });
      this.fillTables(db, localData);
    }, (error) => {
      console.error('No se pudo abrir la BD. ' +error);
    });

  }

  setRoot(root){
    this.tab2Root = root;
  }

  fillTables(db: SQLiteObject, localData) {
    this.auth.initTables().then((result: any) => {
      if (result) {
        let categorias = JSON.parse(JSON.stringify(result.categorias));
        let colores = JSON.parse(JSON.stringify(result.colores));
        let cuerpos = JSON.parse(JSON.stringify(result.cuerpos));
        let recomendaciones = JSON.parse(JSON.stringify(result.recomendaciones));
        let subcat = JSON.parse(JSON.stringify(result.subcat));

        try{
          if(result.categorias.length > localData.categorias){
            for (let item in categorias) {
              db.executeSql("INSERT INTO categorias (id, categoria, imgen, genero) VALUES (?, ?, ?, ?)", [categorias[item].id, categorias[item].categoria, categorias[item].imgen, categorias[item].genero]);
            }
          }
          if(result.colores.length > localData.colores){
            for (let item in colores) {
              db.executeSql("INSERT INTO colores (id, color) VALUES (?, ?)", [colores[item].id, colores[item].color]);
            }
          }
          if(result.cuerpos.length > localData.cuerpos){
            for (let item in cuerpos) {
              db.executeSql("INSERT INTO cuerpos (id, tipo_cuerpo) VALUES (?, ?)", [cuerpos[item].id, cuerpos[item].tipo_cuerpo]);
            }
          }
          if(result.recomendaciones.length > localData.recomendaciones){
            for (let item in recomendaciones) {
              db.executeSql("INSERT INTO recomendaciones (id, nombre) VALUES (?, ?)", [recomendaciones[item].id, recomendaciones[item].nombre]);
            }
          }
          if(result.subcat.length > localData.subcat){
            for (let item in subcat) {
              db.executeSql("INSERT INTO subcategorias (id, categoria_id, nombre) VALUES (?, ?, ?)",
                [subcat[item].id, subcat[item].categoria_id, subcat[item].nombre]);
            }
          }
        } catch(e){
          this.showError('Algo salió mal, intenta más tarde. ' +e);
        }
      } else {
        this.showError('Algo salió mal, intenta más tarde.');
      }
    }, error => {
      this.showError('Algo salió mal con la conexión, intenta más tarde. ' +error);
    });
  }

  logout() {
    let confirm = this.alertCtrl.create({
      title: 'Cerrar sesión',
      message: '¿Estás seguro que desesas cerrar sesión?',
      buttons: [
        {
          text: 'Cancelar'
        },
        {
          text: 'Aceptar',
          handler: () => {
            this.showLoading();
            this.auth.desconectar();
            this.navCtrl.setRoot(HomePage);
          }
        }
      ]
    });
    confirm.present();
  }

  showError(error) {
    this.loading.dismiss();
    let alert = this.alertCtrl.create({
      title: '¡UPS!',
      subTitle: error,
      buttons: ['Aceptar']
    });
    alert.present();
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Cerrando sesión. Por favor espera...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  presentStartModal() {
    let profileModal = this.modalCtrl.create(StartPage);
    profileModal.present();
  }
}
