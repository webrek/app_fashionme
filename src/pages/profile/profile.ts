import { Component } from '@angular/core';
import { NavController, NavParams, Loading, LoadingController, AlertController} from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

import { AuthService } from '../../providers/auth-service';

import { HomePage } from '../home/home';
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  loading: Loading;
  database: SQLite;
  user = {nombre: '', email: '', color: '', cuerpo: '', color_ideal: '', image: ''};

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, private auth: AuthService, private loadingCtrl: LoadingController) {
    let image = localStorage.getItem('imageUser');
    let user = JSON.parse(localStorage.getItem('user'));
    this.user.nombre = user.nombre;
    this.user.email = user.email;
    if(image == null || image == ''){
      this.user.image = 'assets/images/body.png';
    } else {
      this.user.image = localStorage.getItem('imageUser');
    }

    this.database = new SQLite();
    
    this.database.create({name: "fashionme.db", location: "default"}).then((db: SQLiteObject) => {
      db.executeSql("SELECT * FROM cuerpos WHERE id = (?)", [user.cuerpo_id]).then((data) => {
        if(data.rows.length > 0) {
            for(var i = 0; i < data.rows.length; i++) {
                this.user.cuerpo = data.rows.item(i).tipo_cuerpo;
            }
        }
      }, error => {
        console.error('Error del SQL: ' +JSON.stringify(error));
      });
      db.executeSql("SELECT * FROM colores WHERE id = (?)", [user.color_id]).then((data) => {
        if(data.rows.length > 0) {
            for(var i = 0; i < data.rows.length; i++) {
                this.user.color = data.rows.item(i).color;
                this.user.color_ideal = 'assets/images/'+data.rows.item(i).color.toLowerCase()+'.jpg';
            }
        }
      }, error => {
        console.error('Error del SQL: ' +JSON.stringify(error));
      });
    }, error => {
      console.error('Error openDB: ' +JSON.stringify(error));
    });
  }

  logout() {
    let confirm = this.alertCtrl.create({
      title: 'Cerrar sesión',
      message: '¿Estás seguro que desesas cerrar sesión?',
      buttons: [
        {
          text: 'Cancelar'
        },
        {
          text: 'Aceptar',
          handler: () => {
            this.showLoading();
            this.auth.desconectar();
            this.navCtrl.setRoot(HomePage);
          }
        }
      ]
    });
    confirm.present();
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Cerrando sesión. Por favor espera...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

}
