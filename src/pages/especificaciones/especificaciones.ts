import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Colorimetria } from '../colorimetria/colorimetria';

@Component({
  selector: 'page-especificaciones',
  templateUrl: 'especificaciones.html',
})
export class Especificaciones {

  rootPage = Colorimetria;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

}
