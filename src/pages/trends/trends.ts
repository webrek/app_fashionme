import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { AuthService } from '../../providers/auth-service';

@Component({
  selector: 'page-trends',
  templateUrl: 'trends.html',
})
export class TrendsPage {

  resp = {titulo: '', contenido: '' };

  constructor(public navCtrl: NavController, public navParams: NavParams, private auth: AuthService) {
    this.getTrends();
  }

  getTrends(){
    this.auth.getTrends().then((result: any) => {
      console.log(JSON.parse(JSON.stringify(result.trends)));
      this.resp.titulo = result.trends.titulo;
      this.resp.contenido = result.trends.contenido;
    });
  }

}
