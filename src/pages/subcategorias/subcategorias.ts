import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

import { ImagesPage } from '../images/images';

@Component({
  selector: 'page-subcategorias',
  templateUrl: 'subcategorias.html',
})
export class SubcategoriasPage {

  database: SQLite;
  subcategorias = [];
  id: number = this.navParams.get('id');
  categoria: string = this.navParams.get('categoria');
  closet: number = this.navParams.get('closet');

  constructor(public navCtrl: NavController, public navParams: NavParams) {
      this.database = new SQLite();
      this.database.create({name: "fashionme.db", location: "default"}).then((db: SQLiteObject) => {
        db.executeSql("SELECT * FROM subcategorias WHERE categoria_id = (?)", [this.id]).then((data) => {
            for(var i = 0; i < data.rows.length; i++) {
                this.subcategorias.push({id: data.rows.item(i).id, categoria_id: data.rows.item(i).categoria_id, nombre: data.rows.item(i).nombre});
            }
        }, error => {
            console.error('Acurrio un error: ' +error);
        });
    }, (error) => {
        let subcat = [
            {
                "id": 1,
                "categoria_id": 1,
                "nombre": "Jumpsuit"
            },
            {
                "id": 2,
                "categoria_id": 1,
                "nombre": "Romper"
            },
            {
                "id": 3,
                "categoria_id": 1,
                "nombre": "Vestido de día"
            },
            {
                "id": 4,
                "categoria_id": 1,
                "nombre": "Vestido de noche"
            },
            {
                "id": 5,
                "categoria_id": 2,
                "nombre": "Camisas"
            },
            {
                "id": 6,
                "categoria_id": 2,
                "nombre": "Tshirts"
            },
            {
                "id": 7,
                "categoria_id": 2,
                "nombre": "Tank tops"
            },
            {
                "id": 8,
                "categoria_id": 2,
                "nombre": "Blusas"
            },
            {
                "id": 9,
                "categoria_id": 2,
                "nombre": "Sweaters"
            },
            {
                "id": 10,
                "categoria_id": 3,
                "nombre": "Jeans"
            },
            {
                "id": 11,
                "categoria_id": 3,
                "nombre": "Pantalones"
            },
            {
                "id": 12,
                "categoria_id": 3,
                "nombre": "Shorts"
            },
            {
                "id": 13,
                "categoria_id": 3,
                "nombre": "Faldas"
            },
            {
                "id": 14,
                "categoria_id": 4,
                "nombre": "Blazers"
            },
            {
                "id": 15,
                "categoria_id": 4,
                "nombre": "Sacos"
            },
            {
                "id": 16,
                "categoria_id": 4,
                "nombre": "Chamarras"
            },
            {
                "id": 17,
                "categoria_id": 4,
                "nombre": "Abrigos"
            },
            {
                "id": 18,
                "categoria_id": 5,
                "nombre": "Bodysuit"
            },
            {
                "id": 19,
                "categoria_id": 5,
                "nombre": "Brasiere"
            },
            {
                "id": 20,
                "categoria_id": 5,
                "nombre": "Calzones"
            },
            {
                "id": 21,
                "categoria_id": 5,
                "nombre": "Panties"
            },
            {
                "id": 22,
                "categoria_id": 5,
                "nombre": "Hosiery"
            },
            {
                "id": 23,
                "categoria_id": 5,
                "nombre": "Calcetines"
            },
            {
                "id": 24,
                "categoria_id": 5,
                "nombre": "Pijamas"
            },
            {
                "id": 25,
                "categoria_id": 6,
                "nombre": "Bikinis"
            },
            {
                "id": 26,
                "categoria_id": 6,
                "nombre": "Traje de Baño Completo"
            },
            {
                "id": 27,
                "categoria_id": 6,
                "nombre": "Cover Ups"
            },
            {
                "id": 28,
                "categoria_id": 7,
                "nombre": "Zapatos de Tacon"
            },
            {
                "id": 29,
                "categoria_id": 7,
                "nombre": "Botas / Botines"
            },
            {
                "id": 30,
                "categoria_id": 7,
                "nombre": "Sandalias"
            },
            {
                "id": 31,
                "categoria_id": 7,
                "nombre": "Flats"
            },
            {
                "id": 32,
                "categoria_id": 7,
                "nombre": "Tenis Deportivos"
            },
            {
                "id": 33,
                "categoria_id": 8,
                "nombre": "Lentes"
            },
            {
                "id": 34,
                "categoria_id": 8,
                "nombre": "Relojes"
            },
            {
                "id": 35,
                "categoria_id": 8,
                "nombre": "Sombreros y Gorras"
            },
            {
                "id": 36,
                "categoria_id": 8,
                "nombre": "Bufandas"
            },
            {
                "id": 37,
                "categoria_id": 8,
                "nombre": "Joyeria"
            },
            {
                "id": 38,
                "categoria_id": 8,
                "nombre": "Bolsas"
            },
            {
                "id": 39,
                "categoria_id": 8,
                "nombre": "Otros"
            },
            {
                "id": 40,
                "categoria_id": 3,
                "nombre": "Leggins "
            }
        ];
        for (let item in subcat) {
            this.subcategorias.push({id: subcat[item].id, categoria_id: subcat[item].categoria_id, nombre: subcat[item].nombre});
        }
        console.error('No se pudo abrir la BD. ' +error);
    });
  }

  getImages(subcategoria_id, subcat){
      this.navCtrl.push(ImagesPage, {id: subcategoria_id, nombre: subcat, closet: this.closet});
  }

}
