import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Loading } from 'ionic-angular';
import { User } from '../../database';
import { AuthService } from '../../providers/auth-service';
import { Tabs } from '../tabs/tabs';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})

export class Register {
  loader: Loading;
	modelo = new  User("", "", "", "", "", null);
  createSuccess = false;
  errors = {nombre: '', sexo: '', email: '', password: ''};

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private auth: AuthService, 
    private alertCtrl: AlertController, 
    public loadingCtrl: LoadingController) { }

  save(){
    this.presentLoading('Por favor espere...');
    this.modelo.password = btoa(this.modelo.password);
    this.modelo.password_confirmation = btoa(this.modelo.password_confirmation);
    this.auth.registerUser(this.modelo).then((resultados) => {
      // this.loader.dismiss();
      if(resultados['check'] == true){
        localStorage.setItem('currentUser', 'active');
        localStorage.setItem('userToken', resultados['content']);
        localStorage.setItem('user', JSON.stringify(resultados['user']));
        this.navCtrl.setRoot(Tabs);

        this.createSuccess = true;
        this.showPopup('Correcto', 'La cuenta fue creada correctamente');
      } else {
        this.loader.dismiss();
        if(resultados['errors']){
          this.modelo.password = '';
          this.modelo.password_confirmation = '';
          this.errors.nombre = resultados['errors']['nombre'] ? resultados['errors']['nombre']['0'] : '';
          this.errors.sexo = resultados['errors']['sexo'] ? resultados['errors']['sexo']['0'] : '';
          this.errors.email = resultados['errors']['email'] ? resultados['errors']['email']['0'] : '';
          this.errors.password = resultados['errors']['password'] ? resultados['errors']['password']['0'] : '';
        }
      }
    }, (error) => {
      this.showPopup('¡UPS!', 'Algo salió mal con la conexón. Intenta más tarde.');
    });
  }

  presentLoading(text) {
    this.loader = this.loadingCtrl.create({
      content: text
    });
    this.loader.present();
  }

  showPopup(title, text){
    this.loader.dismiss();
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: [{
        text: 'Aceptar',
        handler: data => {
          if(this.createSuccess) {
            this.navCtrl.popToRoot();
          }
        }
    }]
    });
    alert.present();
  }
}
