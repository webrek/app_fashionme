import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

// import db  from '../database';

import { HomePage } from '../pages/home/home';
import { Tabs } from '../pages/tabs/tabs';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
      
    if(localStorage.getItem('currentUser') == "active"){
      this.rootPage = Tabs;
      // this.navCtrl.setRoot(this.rootPage);
    }
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();

      let db = new SQLite();
      db.create({
        name: 'fashionme.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        try{
          db.executeSql('CREATE TABLE IF NOT EXISTS cuerpos(id INTEGER PRIMARY KEY AUTOINCREMENT, tipo_cuerpo TEXT);', {});
          db.executeSql('CREATE TABLE IF NOT EXISTS categorias(id INTEGER PRIMARY KEY AUTOINCREMENT, categoria TEXT, imgen TEXT, genero CHARACTER);', {});
          db.executeSql('CREATE TABLE IF NOT EXISTS colores(id INTEGER PRIMARY KEY AUTOINCREMENT, color TEXT);', {});
          db.executeSql('CREATE TABLE IF NOT EXISTS recomendaciones(id INTEGER PRIMARY KEY AUTOINCREMENT, nombre TEXT);', {});
          db.executeSql('CREATE TABLE IF NOT EXISTS subcategorias(id INTEGER PRIMARY KEY AUTOINCREMENT, categoria_id INTEGER, nombre TEXT);', {});          
        } catch(e){
          console.error("Las tablas no fueron creadas. " +e);
        }
      }).catch( (e) =>{
        console.error('No se pudo crear la BD. ' +e);
      });
  });
  }
}

